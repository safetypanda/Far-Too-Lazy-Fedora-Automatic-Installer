#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root or run script with sudo..."
  exit
fi

echo "*-------------------------------------*"
echo "| FAR TOO LAZY: FEDORA AUTO INSTALLER |"
echo "|             KDE EDITION             |"
echo "| 	Built for Fedora 30         |"
echo "*-------------------------------------*"
echo "        PRESS ENTER TO CONTINUE"


read uselessVar
clear

echo "THIS ONLY WORKS FOR FEDORA 29 OR HYPOTHETICALLY ANY DNF BASED SYSTEM"
echo "USUAL WARNINGS HERE, USE IT ON MY SYSTEM DON'T WORRY BUT STILL CHECK SOURCE CODE ;)"
echo ""
echo "YOU'LL HAVE TO MANUALLY START CLION.SH Command to do so [no sudo needed]: $~/IDEs/clion-2018.3.3/bin/clion.sh "
echo "CHANGE OR MODIFY IF WANTED" 
echo "PRESS ENTER TO CONTINUE"

read uselessVar

################################
## TIME TO DEBLOAT (KDE ONLY) ##
################################
echo "GRAB YOURSELF A KANDY BAR I'LL TAKE IT FROM HERE!"
echo "DEBLOATING YOUR KDE SPIN... IF YOU'RE NOT USING KDE NOTHING BAD WILL HAPPEN. :)"
sleep 3

sudo dnf remove dragonplayer -y
sudo dnf remove k3b -y
sudo dnf remove calligra-words -y
sudo dnf remove calligra-stage -y
sudo dnf remove calligra-sheets -y
sudo dnf remove konqueror -y
sudo dnf remove dnfdragora -y


########################
##  CHECK FOR UPDATES ##
########################
echo "CHECKING FOR UPDATES!"
sleep 3
sudo dnf upgrade -y

##############################
## Time To Install Packages ##
##############################
# IF WANTING TO NOT INSTALL THESE COMMENT OUT
# TO ADD JUST ADD YOUR INSTALL COMMAND FOLLOWED BY A -y

echo "INSTALLING PACKAGES..."
sleep 3

sudo dnf install neofetch -y
sudo dnf install eclipse -y
sudo dnf install android-tools -y
sudo dnf install htop -y
sudo dnf install nano -y
sudo dnf install libreoffice-writer -y
sudo dnf install terminus-fonts -y
sudo dnf install zsh -y
sudo dnf install git -y
sudo dnf install gcc -y 
sudo dnf install ruby -y

#############################
## NON FREE PACKAGES BELOW ##
#############################

echo "ENABLING RPM-FUSION REPOS"
sleep 3

sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm 

sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

########################################################
## ENABLING FLATHUB & INSTALLING GITKRAKEN & DISCORD  ##
########################################################
echo "ENABLING FLATHUB & INSTALL GITKRAKEN"
sleep 3

sudo dnf install flatpak -y

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.axosoft.GitKraken
flatpak install flathub com.discordapp.Discord
#flatpak install flathub com.slack.Slack #In case people want slack. I don't use it.

#####################################
## MANUALLY INSTALL SOME THINGS :( ##
#####################################
echo "DOWNLOADING, UNPACKING AND STARTING CLION"
sleep 3
mkdir IDEs
cd IDEs
wget https://download.jetbrains.com/cpp/CLion-2018.3.3.tar.gz
tar -xvzf CLion-2018.3.3.tar.gz
cd ..
chmod 755 clion-2018.3.3
cd ..
chmod 755 IDEs

################################
## DOING SOME... ENHANCEMENTS ##
################################
echo "Faster IO Speeds!"
sleep 3

ssd_scheduler="deadline"
hdd_scheduler="deadline"

cat <<EOF | tee "/etc/udev/rules.d/60-io_schedulers.rules" > /dev/null 2>&1
# Set deadline scheduler for non-rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="$ssd_scheduler"
# Set deadline scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="$hdd_scheduler"
EOF

for disk in /sys/block/sd*; do
    rot="$disk/queue/rotational"
    sched="$disk/queue/scheduler"

    if [[ $(cat "$rot") -eq 0 ]]; then
        echo "$ssd_scheduler | tee $sched > /dev/null 2>&1"
    elif [[ $(cat "$rot") -eq 1 ]]; then
        echo "$hdd_scheduler | tee $sched > /dev/null 2>&1"
    fi
done

#
# HARDEN FEDORA?
#
clear && echo "HARDEN-ISH YOUR FEDORA INSTALL?"
read answer

if [[ $answer = "y" ]]
then
    echo "Hardening SSH"
    
    sudo echo "" >> /etc/ssh/sshd_config
    sudo echo "#AUTOMATED HARDENING FROM FTLFAI" >> /etc/ssh/sshd_config
    sudo  echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
    sudo echo "ClientAliveCountMax 0" >> /etc/ssh/sshd_config
    sudo echo "ClientAliveInterval 300" >> /etc/ssh/sshd_config
     sudo echo "PermitRootLogin no" >> /etc/ssh/sshd_config
    

    echo "Enabling Firewall" #MShould be enabled already
    sudo systemctl enable firewalld.service
fi

##############################
##   CLEANING UP/REBOOTING  ##
##############################
clear
echo "CLEANING AND REMOVING TEMP FILES..."
sudo dnf clean all

echo "REBUILDING REPOSITORIES..."
sudo dnf update

clear && neofetch && echo "DONE! :) DO YOU WANT TO REBOOT?(y/n)"
read answer

if [[ $answer = "y" ]]
then
reboot
fi







