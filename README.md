# Far Too Lazy: Fedora Automatic Installer
- FTLFAI Built for Fedora 30

# ABOUT:
I have a habit of re-installing Fedora with every new release, so this script is designed to make my life easier. I share it with newcomers to Fedora from my college who have an interest in Linux for Computer Science Classes, Programming or just everyday use.

This does not turn off SELinux or set it to permissive. You shouldn't turn it off anyways.
This is mainly catered around my needs and even still doesn't install everything I use, got my own personal script for that. So obviously change it to suite yours. 

# VERSION DIFFERENCES:
- Workstation Edition
    - Nothing removed.
- Cinnamon Edition
    - Removes dnfdragora, brasero, pidgin, xawTV
- KDE Edition
    - Removes: dnfdragora, Konqueror, DragonPlayer, Calligra (replaced with libreoffice-writer), dnfdragora and k3b

# What Does It Install and do?
- neofetch
- eclipse
- Latest openjdk
- android-tools
- htop
- nano 
- gnome-tweak-tool (WORKSTATION ONLY)
- CLion
- gitkraken [flatpak]
- discord [flatpak]
- zsh
- ruby
- gcc

It also applies some performance tweaks and automates some hardening [Currently SSH only at the moment] following various hardening guides. Note that if you are wanting to harden your Fedora install, make sure you encrypt your hard drive first, keep installed packages/software small, and even with this applied don't trust that it will keep you 100% safe.

# USAGE:
- Download script, put yourself in your `/~` or `/home/$USER/` folder. Allow it to be executed. Run it with sudo or su. Get some coffee can take some time.

# WHAT NEXT?
- Updating script when Fedora 31 comes out. 

# LICENSE:
Copyright (C) 2017 - 2019  James Gillman [jronaldgillman@gmail.com]
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.
